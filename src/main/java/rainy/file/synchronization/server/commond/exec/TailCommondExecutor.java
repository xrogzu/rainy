package rainy.file.synchronization.server.commond.exec;

import rainy.file.synchronization.config.RainyRunTime;
import rainy.file.synchronization.server.commond.Command;
import rainy.file.synchronization.server.commond.CommondExecutor;
import rainy.file.synchronization.server.file.TailerAppender;

/**
 * 
 * @author dongwenbin
 *
 */
public class TailCommondExecutor implements CommondExecutor<String> {

	public String supportedCommond() {
		return RainyRunTime.COMMOND_TAIL;
	}

	public void executeCommand(Command<String> command) {
		TailerAppender.evalCommand(command.getFullContent());
	}

}
